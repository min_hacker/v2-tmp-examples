const path = require('path');
const BASE_URL = process.env.NODE_ENV === "production" ? "./" : "/";
module.exports = {
  outputDir: 'dist',
  publicPath: BASE_URL,
  lintOnSave: true,
  productionSourceMap: process.env.NODE_ENV === "production" ? false : true,
  devServer: {
    publicPath: BASE_URL,
  },
  chainWebpack(config) {
    config.plugin('copy').use(require('copy-webpack-plugin'), [
      {
        patterns: [
          {
            from: path.resolve(`src/static`),
            to: `${path.resolve('dist')}/static`
          },
        ]
      }
    ])
  }
};