import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/assets/css/tailwind.css";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "@/Mock";
import "animate.css";
import "@/assets/css/index.scss"
import directive from "@/directive";
import ol from '@/static/openlayers/ol.js';
import '@/static/openlayers/ol.css';

window.ol = ol;
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(directive);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
