// <el-drawer v-draw:classList="{head: 'el-drawer__header', wrap: 'el-drawer__container'}"><el-drawer> 传入抓捕获的{head: '', wrap: ''}
export default {
  bind(el, binding, vnode, oldVnode) {
    let { head, wrap } = binding.value || {};
    !head && (head = 'el-dialog__header');
    !wrap && (wrap = 'el-dialog');
    const HeaderEl = el.querySelector('.' + head);
    const dragDom = el.querySelector('.' + wrap);
    if (!HeaderEl && !dragDom) throw "dom not defined";
    HeaderEl.style.cursor = 'move';
    HeaderEl.style.userSelect = 'none';
    // 获取原有属性 ie dom元素.currentStyle 火狐谷歌 window.getComputedStyle(dom元素, null);
    const sty = dragDom.currentStyle || window.getComputedStyle(dragDom, null);
    let width = dragDom.style.width;
    if (width.includes('%')) {
      width = +document.body.clientWidth * (+width.replace(/\%/g, '') / 100);
    } else {
      width = +width.replace(/\px/g, '');
    }
    dragDom.style.position = 'absolute';
    dragDom.style.top = `150px`;
    if (head.includes('dialog')) {
      dragDom.style.marginTop = 0;
      dragDom.style.left = `${(document.body.clientWidth - width) / 2}px`;
    }
    HeaderEl.onmousedown = (e) => {
      // 鼠标按下，计算当前元素距离可视区的距离 (鼠标点击位置距离可视窗口的距离)
      const disX = e.clientX - HeaderEl.offsetLeft;
      const disY = e.clientY - HeaderEl.offsetTop;

      let styL, styT;// 获取到的值带px 正则匹配替换

      if (sty.left.includes('%')) {// 注意在ie中 第一次获取到的值为组件自带50% 移动之后赋值为px
        styL = +document.body.clientWidth * (+sty.left.replace(/\%/g, '') / 100);
        styT = +document.body.clientHeight * (+sty.top.replace(/\%/g, '') / 100);
      } else {
        styL = +sty.left.replace(/\px/g, '');
        styT = +sty.top.replace(/\px/g, '');
      };
      document.onmousemove = (ev) => {
        // 通过事件委托，计算移动的距离 （开始拖拽至结束拖拽的距离）
        let l = ev.clientX - disX;
        const t = ev.clientY - disY;

        let finallyL = l + styL
        let finallyT = t + styT
        const left = -(dragDom.clientWidth - dragDom.clientWidth / 10);
        const right = document.body.clientWidth - dragDom.clientWidth / 10;
        const bottom = document.body.clientHeight - dragDom.clientWidth / 10;
        finallyL = finallyL < left ? left : finallyL;
        finallyL = finallyL > right ? right : finallyL;
        finallyT = finallyT < 0 ? 0 : finallyT;
        finallyT = finallyT > bottom ? bottom : finallyT;
        // 移动当前元素
        dragDom.style.left = `${finallyL}px`;
        dragDom.style.top = `${finallyT}px`;

      };

      document.onmouseup = function (e) {
        document.onmousemove = null;
        document.onmouseup = null;
      };
    };
  },
}