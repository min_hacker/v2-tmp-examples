import drag from "./libs/drag";

export default (Vue) => {
  Vue.directive('drag', drag)
}