export function format(limit) {
  let size = "";
  if (limit < 1 * 1024) { //如果小于0.1KB转化成B
    size = limit.toFixed(2) + "B";
  } else if (limit < 1 * 1024 * 1024) { //如果小于0.1MB转化成KB
    size = (limit / 1024).toFixed(2) + "KB";
  } else if (limit < 1 * 1024 * 1024 * 1024) { //如果小于0.1GB转化成MB
    size = (limit / (1024 * 1024)).toFixed(2) + "MB";
  } else { //其他转化成GB
    size = (limit / (1024 * 1024 * 1024)).toFixed(2) + "GB";
  }

  let sizestr = size + "";
  let len = sizestr.indexOf("\.");
  let dec = sizestr.substr(len + 1, 2);
  if (dec == "00") { //当小数点后为00时 去掉小数部分
    return sizestr.substring(0, len) + sizestr.substr(len + 3, 2);
  }
  return sizestr;
}

export default [
  {
    "Size": 5735331,
    "ip": "192.168.1.10",
    "Count": 11140
  },
  {
    "Size": 1837157,
    "ip": "192.168.1.194",
    "Count": 1279
  },
  {
    "Size": 710933,
    "ip": "192.168.1.21",
    "Count": 4180
  },
  {
    "Size": 292893,
    "ip": "192.168.1.11",
    "Count": 2318
  },
  {
    "Size": 87271,
    "ip": "34.208.213.149",
    "Count": 187
  },
  {
    "Size": 80015,
    "ip": "192.168.1.195",
    "Count": 446
  },
  {
    "Size": 39732,
    "ip": "192.168.1.102",
    "Count": 152
  },
  {
    "Size": 39503,
    "ip": "192.168.1.178",
    "Count": 177
  },
  {
    "Size": 30112,
    "ip": "192.168.1.191",
    "Count": 175
  },
  {
    "Size": 23405,
    "ip": "192.168.1.12",
    "Count": 160
  }
]