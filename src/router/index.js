import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import About from "../views/About.vue";
import Chart from "../views/Chart.vue";
import Other from "@/views/Other.vue";
import Maps from "@/views/map/index.vue";
import Layer from "@/views/layer/index.vue";
import Windmail from "@/views/windmail";
Vue.use(VueRouter);

const routes = [
  { path: "/", redirect: "/Other" },
  { path: "/login", name: "Login", component: Login, },
  { path: "/home", name: "Home", component: Home, },
  { path: "/about", name: "About", component: About, },
  { path: "/chart", name: "Chart", component: Chart, },
  { path: "/other", name: "Other", component: Other, },
  { path: "/maps", name: "Maps", component: Maps, },
  { path: "/layer", name: "Layer", component: Layer, },
  { path: "/windmail", name: "Windmail", component: Windmail, },
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
