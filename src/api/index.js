import request, { Amap } from "./request";
import { Amapkey } from "@/views/map/utils/index.js";
export const commentList = (params) => request({url: '/api/comment/list', method: 'post', params });
// https://lbs.amap.com/api/webservice/guide/api/district
export const getBoundary = params => Amap({url: "/v3/config/district", method: "post", params: {...params, key: Amapkey}});
