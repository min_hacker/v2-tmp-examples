import ol from '@/static/openlayers/ol.js';
export const EPSG3857 = 'EPSG:3857';
export const EPSG4326 = 'EPSG:4326';

export const createRegularPolygon =  ol.interaction.Draw.createRegularPolygon(4);

export const createRectangle =  ol.interaction.Draw.createBox();

export const initDrawElements = {
  //根据类型生成要素
  FeatureFactory: (featureTpye,coordinate) => {
    switch(featureTpye){
      case 'Point':{
        return new ol.Feature({
          geometry: new ol.geom.Point(coordinate)
        })
      }         
      case 'LineString':{
        return new ol.Feature({
          geometry: new ol.geom.LineString([coordinate[0],coordinate[1]])
        })
      }
      case 'Circle': {
        let circleIn3857 = new ol.geom.Circle(transform(coordinate[0], 'EPSG:4326', 'EPSG:3857'),coordinate[1],'XY');
        let circleIn4326 = circleIn3857.transform('EPSG:3857','EPSG:4326');
        return new ol.Feature({
          geometry: circleIn4326
        })
      }
      case 'Square': {
        let circleIn3857 = new ol.geom.Circle(transform(coordinate[0], 'EPSG:4326', 'EPSG:3857'), coordinate[1],'XY');
        let circleIn4326 = circleIn3857.transform('EPSG:3857','EPSG:4326');
        let Square = new ol.geom.Polygon.fromCircle(circleIn4326, 4 ,150)
        return new ol.Feature({
          geometry:  Square
        })
      }
      case 'Rectangle':{
        let Rectangle = new ol.geom.Polygon.fromExtent(coordinate)
        let ret = null
        try{
          ret = new ol.Feature({
            geometry: Rectangle
          })
          return ret;
        }
        catch(ex){
          this.$message.error(ex)
          return;
        }

      }
      case 'Polygon':{
        let Polygon = new ol.Polygon([coordinate])
        return new olFeature({
          geometry: Polygon
        })
      }
    }
  },
  /**
   * 根据类型生成样式
   * 后面可以封装成一个函数，根据用户的输入生成各种要素的类型
   *  */
  StyleFactory: (featureTpye, label) => {
    switch(featureTpye) {
      case 'Point': {
        return new ol.style.Style({
          // image: new ol.style.Circle({
          //   radius: 10,
          //   fill: new ol.style.Fill({ color: "red", opacity: 1 })
          // }),
          text: new ol.style.Text({ //文本样式
            font: '12px Calibri,sans-serif',
            text: label,
            fill: new ol.style.Fill({
              color: 'white'
            }),
            offsetY: -55,
            stroke: new ol.style.Stroke({
              color: 'red',
              width: 2
            }),
          }),
          image: new ol.style.Icon({
              anchor: [0.5, 80],
              anchorXUnits: 'fraction',
              anchorYUnits: 'pixels',
              scale: .6,
              src: require('./img/8.png')
            })
        })
        
      }
      case 'LineString': {
        return new ol.style.Style({
           //填充色
           fill: new Fill({
             color: 'rgba(255, 255, 255, 0.2)'
           }),
           //边线颜色
           stroke: new ol.style.Stroke({
              color: '#0099FF',
              width: 5
           }),
           //形状
           image: new ol.style.Circle({
              radius: 7,
              fill: new ol.style.Fill({
                  color: '#ffcc33'
              })
           })
        })
      }
      case 'Circle': {
        return new ol.style.Style({
          //填充色
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.5)'
          }),
          //边线颜色
          stroke: new ol.style.Stroke({
              color: '#0099FF',
              width: 6
          }),
          //形状
          image: new ol.style.Circle({
              radius: 7,
              fill: new Fill({
                  color: '#ffcc33'
              })
          })
        })
      }
      case 'Square': {
        return new ol.style.Style({
          //填充色
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.8)'
          }),
          //边线颜色
          stroke: new ol.style.Stroke({
              color: 'red',
              width: 2
          }),
          //形状
          image: new ol.style.Circle({
              radius: 7,
              fill: new Fill({
                  color: '#0099FF'
              })
          })
        })
      }
      case 'Polygon': {
        return new ol.style.Style({
          //填充色
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.8)'
          }),
          //边线颜色
          stroke: new ol.style.Stroke({
              color: '#0099FF',
              width: 2
          }),
          //形状
          image: new ol.style.Circle({
              radius: 7,
              fill: new ol.style.Fill({
                  color: '#0099FF'
              })
          })
        })
      }
    }
  },

  DrawVectorSource : () => new ol.source.Vector({wrapX: false}),

  LayerVector:(name, style) => new ol.layer.Vector({name, style})
}
export const interactionDrawFunc = (source, data) => {
  return new ol.interaction.Draw({
    source,
    type: data.type,
    geometryFunction: data.geometryFunction,
    maxPoints: data.maxPoints,
    style: data.style
 })
}
export function __checkLayerVector(map, layer, source) {
  if (!this[source]) this[source] = initDrawElements.DrawVectorSource();
  if (!this[layer]) {
    this[layer] = initDrawElements.LayerVector(layer);
    this[layer].setSource(this[source]);
    map.addLayer(this[layer]);
  };
  return {
    layer: this[layer],
    source: this[source]
  };
}
export function __findLayer(map, attr, val) {
  const layers = map.getLayers().getArray();
  return layers.find(item => item.get(attr) === val);
}
export function __checkInteractDraw(map, draw, source, shape, style) {
  let geometryFunction = undefined;
  if (this[draw] !== null) map.removeInteraction(this[draw]);
  if (shape === 'Square' || shape === 'LineString') geometryFunction = createRectangle;
  this[draw] = interactionDrawFunc(this[source], {type: shape, geometryFunction, style});
  map.addInteraction(this[draw]);
  return this[draw];
}