export const fromLonLat = (extent) => ol.proj.fromLonLat(extent);

export const toLonLat = (extent) => ol.proj.toLonLat(extent);