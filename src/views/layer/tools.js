export const newId = function () {
  var mydate = new Date();
  var result = mydate.getFullYear().toString() + (mydate.getMonth() + 1).toString() + mydate.getDate().toString() + mydate.getHours().toString() + mydate.getMinutes().toString() + mydate.getSeconds().toString() + mydate.getMilliseconds().toString();
  return result;
};
export const addFeatureToLocalstorage = function (feature) {
  var featureList = localstorageGet('featureList');
  if (!featureList) {
    featureList = [];
    localstorageSet('featureList', [])
  }
  var saveFeature = null;
  var geom = feature.getGeometry();
  var type = geom.getType();
  var coordinates = [];
  if (geom instanceof ol.geom.Point) {
    coordinates = [geom.getCoordinates()]
  } else if (geom instanceof ol.geom.LineString) {
    coordinates = geom.getCoordinates()
  } else if (geom instanceof ol.geom.Polygon) {
    coordinates = geom.getCoordinates()
  } else if (geom instanceof ol.geom.Circle) {
    coordinates = [geom.getCenter(), geom.getRadius()]
  }
  saveFeature = {
    id: feature.getId(),
    name: feature.get('name'),
    remark: feature.get('remark'),
    type: type,
    coordinates: coordinates,
    style: feature.get('style')
  };
  if (saveFeature) {
    var existed = false;
    for (var index = 0; index < featureList.length; index++) {
      var element = featureList[index];
      if (element.id == saveFeature.id) {
        featureList[index] = saveFeature;
        existed = true;
        break
      }
    }
    if (!existed) {
      featureList.push(saveFeature)
    }
    localstorageSet('featureList', featureList)
  }
}
export function localstorageSet(key, obj) {
  if (obj) {
    var save = JSON.stringify(obj);
    localStorage.setItem(key, save);
  }
}
export function localstorageGet(key) {
  var out = null;
  if (key) {
    var data = localStorage.getItem(key);
    if (data) {
      out = JSON.parse(data);
    }
  }
  return out;
}
/**
 * 字符串换行处理
 */
 export function stringDivider(str, width, spaceReplacer) {
  if (!str) {
      return '';
  }
  if (str.length > width) {
      var p = width;
      while (p > 0 && (str[p] != ' ' && str[p] != '-')) {
          p--;
      }
      if (p > 0) {
          var left;
          if (str.substring(p, p + 1) == '-') {
              left = str.substring(0, p + 1);
          } else {
              left = str.substring(0, p);
          }
          var right = str.substring(p + 1);
          return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
      }
  }
  return str;
}