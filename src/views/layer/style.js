
import ol from '@/static/openlayers/ol.js';
import { stringDivider } from "./tools"
const fill = (color) => new ol.style.Fill({ color });
const stroke = (color, width, lineDash = [0, 0]) => new ol.style.Stroke({ color, width, lineDash });
const circle = (radius, fill) => new ol.style.Circle({ radius, fill });
const icon = (src, scale, anchor = [0, 0]) => new ol.style.Icon({ src, scale, anchor });
const moveIcon = icon('./img/move.png', .5);
export const markLayerStyle = (feature) => {
  var style = JSON.parse(feature.get('style'));
  var createTextStyle = function (feature) {
    return new ol.style.Text({
      textAlign: "center", textBaseline: "top", font: 'normal ' + style.label.fontSize + 'px ' + 'Arial', text: stringDivider(feature.get('name'), 16, '\n'),
      fill: fill(style.label.fontColor), stroke: stroke("#fff", 3), offsetX: 0, offsetY: 12, rotation: 0
    })
  };
  var styles = {};
  styles['Polygon'] = [new ol.style.Style({ fill: fill(style.polygon.fillColor), stroke: stroke(style.polygon.borderColor, parseFloat(style.polygon.borderWidth)), text: createTextStyle(feature) })];
  styles['Circle'] = styles['Polygon'];
  styles['MultiPolygon'] = styles['Polygon'];
  styles['LineString'] = [new ol.style.Style({ stroke: stroke(style.line.lineColor, parseFloat(style.line.lineWidth)), text: createTextStyle(feature) })];
  styles['MultiLineString'] = styles['LineString'];
  styles['Point'] = [new ol.style.Style({ image: icon(style.point.icon, .5), text: createTextStyle(feature) })];
  styles['MultiPoint'] = styles['Point'];
  styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
  return styles[feature.getGeometry().getType()]
}

export const drawStyleFunc = function (type, markStyle) {
  const { point, line, polygon, } = markStyle;
  const { lineColor, lineWidth } = line;
  const { fillColor, borderColor, borderWidth } = polygon;
  switch (type) {
    case "Point":
      return new ol.style.Style({ image: icon(point.icon, .5, [0.5, 1]), zIndex: 100000 });
    case "LineString":
      return new ol.style.Style({ stroke: stroke(lineColor, parseFloat(lineWidth)) });
    default:
      return new ol.style.Style({ fill: fill(fillColor), stroke: stroke(borderColor, parseFloat(borderWidth)) });
  }
}

export const markStyle = {
  label: { fontSize: 12, fontColor: 'rgba(170,51,0,1)', fontName: 'Arial' },
  point: { icon: './utils/8.png' },
  line: { lineColor: 'rgba(51,153,204,1)', lineWidth: 2 },
  polygon: { borderColor: 'rgba(51,153,204,1)', borderWidth: 2, fillColor: 'rgba(255,255,255,0.5)' }
};

export const style_modify = function () {
  return [new ol.style.Style({
    image: circle(5, fill('rgba(255,255,255,0.4)'), stroke('red', 1.25)),
    fill: fill('rgba(255,255,255,0.4)'),
    stroke: stroke('red', 1.25)
  })]
};
export const style_selected_modify = new function () {
  var styles = {};
  styles['Polygon'] = [new ol.style.Style({ fill: fill([255, 255, 255, 0.5]) }),
  new ol.style.Style({ stroke: stroke([255, 255, 255, 1], 3) }),
  new ol.style.Style({ stroke: stroke([255, 0, 0, 1], 2) }),
  new ol.style.Style({
    image: circle(3, fill("red")),
    geometry: function (feature) {
      var coordinates = [];
      var geo;
      if (feature.getGeometry() instanceof ol.geom.Circle) {
        var geo = feature.getGeometry();
        coordinates = [geo.getCenter(), geo.getLastCoordinate()]
      } else {
        geo = feature.getGeometry();
        coordinates = geo.getCoordinates()[0]
      }
      return new ol.geom.MultiPoint(coordinates)
    }
  })];
  styles['Circle'] = styles['Polygon'];
  styles['MultiPolygon'] = styles['Polygon'];
  styles['LineString'] = [new ol.style.Style({ stroke: stroke([255, 255, 255, 1], 3) }),
  new ol.style.Style({ stroke: stroke([255, 0, 0, 1], 2) }),
  new ol.style.Style({
    image: circle(3, fill("red")),
    geometry: function (feature) {
      var coordinates = feature.getGeometry().getCoordinates();
      return new ol.geom.MultiPoint(coordinates)
    }
  })];
  styles['MultiLineString'] = styles['LineString'];
  styles['Point'] = [new ol.style.Style({ image: moveIcon, zIndex: 100000 })];
  styles['MultiPoint'] = styles['Point'];
  styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
  return function (feature) {
    return styles[feature.getGeometry().getType()]
  }
};
export const style_selected = new function () {
  var styles = {};
  styles['Polygon'] = [
    new ol.style.Style({ fill: fill([255, 255, 255, 0.5]) }),
    new ol.style.Style({ stroke: stroke([255, 255, 255, 1], 5) }),
    new ol.style.Style({ stroke: stroke([255, 0, 0, 1], 3) }),
    new ol.style.Style({
      image: circle(2, fill("red")),
      geometry: (feature) => {
        var coordinates = [];
        var geo;
        if (feature.getGeometry() instanceof ol.geom.Circle) {
          geo = feature.getGeometry();
          coordinates = [geo.getCenter(), geo.getLastCoordinate()]
        } else {
          geo = feature.getGeometry();
          coordinates = geo.getCoordinates()[0]
        }
        return new ol.geom.MultiPoint(coordinates)
      }
    })];
  styles['Circle'] = styles['Polygon'];
  styles['MultiPolygon'] = styles['Polygon'];
  styles['LineString'] = [
    new ol.style.Style({ stroke: stroke([255, 255, 255, 1], 5) }),
    new ol.style.Style({ stroke: stroke([255, 0, 0, 1], 3) }),
    new ol.style.Style({
      image: circle(2, fill("red")),
      geometry: (feature) => {
        var coordinates = feature.getGeometry().getCoordinates();
        return new ol.geom.MultiPoint(coordinates)
      }
    })];
  styles['MultiLineString'] = styles['LineString'];
  styles['Point'] = [new ol.style.Style({ image: circle(5, fill("red")) })];
  styles['MultiPoint'] = styles['Point'];
  styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
  return (feature) => {
    return styles[feature.getGeometry().getType()]
  }
};