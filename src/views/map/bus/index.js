import Vue from 'vue'
export default new Vue();

export const FLY_DISTRICTS = "FLY_DISTRICTS"; // 移动至...

export const FEATURE_EFFECT = "FEATURE_EFFECT"; // 改变wms点显示状态

export const SPACE_QUERY = "SPACE_QUERY"; // 空间查询

export const RANDOM_POINT = "RANDOM_POINT"; // 随机点模拟

