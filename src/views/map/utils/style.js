import ol from '@/static/openlayers/ol.js';
import { stringDivider } from './transfrom';

const fill = (color) => new ol.style.Fill({ color });
const stroke = (color, width, lineDash = [0, 0]) => new ol.style.Stroke({ color, width, lineDash });
const circle = (radius, fill) => new ol.style.Circle({ radius, fill });
const icon = (src, scale, anchor = [0, 0]) => new ol.style.Icon({ src, scale, anchor });
const redIconStyle = new ol.style.Icon({ anchor: [0.5, 10], anchorXUnits: 'fraction', anchorYUnits: 'pixels', scale: .85, src: require('./img/red-mark.png') });
const blueIconStyle = new ol.style.Icon({ anchor: [0.5, 10], anchorXUnits: 'fraction', anchorYUnits: 'pixels', scale: .85, src: require('./img/blue-mark.png') });
const orangeIconStyle = new ol.style.Icon({ anchor: [0.5, 10], anchorXUnits: 'fraction', anchorYUnits: 'pixels', scale: .85, src: require('./img/orange-mark.png') });

export const city_polygon = new ol.style.Style({ stroke: new ol.style.Stroke({ color: "blue", width: 2.5 }), fill: new ol.style.Fill({ color: 'rgba(255, 255, 255, 0.3)' }) });

export const district_polygon = (feature) => {
  let name = feature.id_;
  var font = new ol.style.Text({
    font: '14px Calibri,sans-serif',
    text: name.replace(/\d/, ""),
    fill: new ol.style.Fill({ color: 'white' }),
    offsetY: -15,
    stroke: new ol.style.Stroke({ color: 'red', width: 2 }),
  });
  return new ol.style.Style({
    stroke: new ol.style.Stroke({ color: "red", width: 2.5 }),
    fill: new ol.style.Fill({ color: 'rgba(255, 255, 255, 0.5)' }),
    text: font
  });
}
export const dwar_style = (feature) => {
  var style = {
    label: { fontSize: 12, fontColor: 'rgba(170,51,0,1)', fontName: 'Arial' },
    point: { icon: require("./img/8.png") },
    line: { lineColor: 'rgba(51,153,204,1)', lineWidth: 2 },
    polygon: { borderColor: 'rgba(51,153,204,1)', borderWidth: 2, fillColor: 'rgba(255,255,255,0.5)' }
  };
  var styles = {};
  styles['Polygon'] = [new ol.style.Style({ fill: fill(style.polygon.fillColor), stroke: stroke(style.polygon.borderColor, parseFloat(style.polygon.borderWidth))})];
  styles['Circle'] = styles['Polygon'];
  styles['MultiPolygon'] = styles['Polygon'];
  styles['LineString'] = [new ol.style.Style({ stroke: stroke(style.line.lineColor, parseFloat(style.line.lineWidth))})];
  styles['MultiLineString'] = styles['LineString'];
  styles['Point'] = [new ol.style.Style({ image: new ol.style.Icon({src: style.point.icon, scale: 0.5 }) })];
  styles['MultiPoint'] = styles['Point'];
  styles['GeometryCollection'] = styles['Polygon'].concat(styles['Point']);
  return styles[feature.getGeometry().getType()];
}

export const wfs_point = (feature) => {
  var size = feature.get("features") ? feature.get("features").length : Number;
  var type = feature.get("钻孔类别");
  var label = feature.get("井名");
  var font = new ol.style.Text({
    font: '12px Calibri,sans-serif',
    text: label,
    fill: new ol.style.Fill({ color: 'white' }),
    offsetY: -15,
    stroke: new ol.style.Stroke({ color: 'red', width: 2 }),
  });
  if (type == "参数井") {
    return new ol.style.Style({
      // image: new ol.style.Circle({ radius: 7, fill: new ol.style.Fill({ color: "#3885ff", opacity: 1 }) }),
      // stroke: new ol.style.Stroke({ color: "white", width: 1 }),
      text: feature.get("features") ? new ol.style.Text({ text: size.toString(), fill: new ol.style.Fill({ color: "#fff" }) }) : font,
      image: orangeIconStyle
    });
  }
  else if (type == "调查井") {
    return new ol.style.Style({
      image: new ol.style.Circle({ radius: 7, fill: new ol.style.Fill({ color: "red", opacity: 1 }) }),
      stroke: new ol.style.Stroke({ color: "white", width: 1 }),
      text: feature.get("features") ? new ol.style.Text({ text: size.toString(), fill: new ol.style.Fill({ color: "#fff" }) }) : font,
      image: redIconStyle
    });
  }
  return new ol.style.Style({
    image: new ol.style.Circle({ radius: 7, fill: new ol.style.Fill({ color: "#3885ff", opacity: 1 }) }),
    stroke: new ol.style.Stroke({ color: "white", width: 1 }),
    text: feature.get("features") ? new ol.style.Text({ text: size.toString(), fill: new ol.style.Fill({ color: "#fff" }) }) : font,
    image: blueIconStyle
  });

}

export const flyTo_style = new ol.style.Style({ stroke: new ol.style.Stroke({ color: '#F75000', width: 2, lineDash: [1,2,3,4,5,6] }) })

export const pointAnimation_style = (radius, opacity) => new ol.style.Style({ image: new ol.style.Circle({ radius, stroke: new ol.style.Stroke({ color: 'rgba(255, 31, 0, ' + opacity + ')', width: 0.25 + opacity }) }) })