import ol from '@/static/openlayers/ol.js';
import { EPSG3857, EPSG4326 } from ".";
const getRange = (cC, cB, T) => {
  if (cB !== null) {
    cC = Math.max(cC, cB);
  }
  if (T !== null) {
    cC = Math.min(cC, T)
  }
  return cC;
}

const getLoop = (cC, cB, T) => {
  if (cC > T) {
    cC -= T - cB;
  }
  if (cC < cB) {
    cC += T - cB;
  }
  return cC;
}

const convertor = (cC, cD) => {
  if (!cC || !cD) {
    return null;
  }
  let T = cD[0] + cD[1] * Math.abs(cC.x);
  const cB = Math.abs(cC.y) / cD[9];
  let cE = cD[2] + cD[3] * cB + cD[4] * cB * cB +
    cD[5] * cB * cB * cB + cD[6] * cB * cB * cB * cB +
    cD[7] * cB * cB * cB * cB * cB +
    cD[8] * cB * cB * cB * cB * cB * cB
  T *= (cC.x < 0 ? -1 : 1);
  cE *= (cC.y < 0 ? -1 : 1);
  return [T, cE];
}

export const convertLL2MC = (T) => {
  const LLBAND = [75, 60, 45, 30, 15, 0];
  const LL2MC = [
    [-0.0015702102444, 111320.7020616939, 1704480524535203, -10338987376042340, 26112667856603880, -35149669176653700, 26595700718403920, -10725012454188240, 1800819912950474, 82.5],
    [0.0008277824516172526, 111320.7020463578, 647795574.6671607, -4082003173.641316, 10774905663.51142, -15171875531.51559, 12053065338.62167, -5124939663.577472, 913311935.9512032, 67.5],
    [0.00337398766765, 111320.7020202162, 4481351.045890365, -23393751.19931662, 79682215.47186455, -115964993.2797253, 97236711.15602145, -43661946.33752821, 8477230.501135234, 52.5],
    [0.00220636496208, 111320.7020209128, 51751.86112841131, 3796837.749470245, 992013.7397791013, -1221952.21711287, 1340652.697009075, -620943.6990984312, 144416.9293806241, 37.5],
    [-0.0003441963504368392, 111320.7020576856, 278.2353980772752, 2485758.690035394, 6070.750963243378, 54821.18345352118, 9540.606633304236, -2710.55326746645, 1405.483844121726, 22.5],
    [-0.0003218135878613132, 111320.7020701615, 0.00369383431289, 823725.6402795718, 0.46104986909093, 2351.343141331292, 1.58060784298199, 8.77738589078284, 0.37238884252424, 7.45]
  ]
  let cD, cC, len;
  T.x = getLoop(T.x, -180, 180);
  T.y = getRange(T.y, -74, 74);
  const cB = T;
  for (cC = 0, len = LLBAND.length; cC < len; cC++) {
    if (cB.y >= LLBAND[cC]) {
      cD = LL2MC[cC];
      break
    }
  }
  if (!cD) {
    for (cC = LLBAND.length - 1; cC >= 0; cC--) {
      if (cB.y <= -LLBAND[cC]) {
        cD = LL2MC[cC];
        break;
      }
    }
  }
  const cE = convertor(T, cD);
  return cE;
}

export const mercatorTolonlat = (x, y) => {
  var x = x / 20037508.34 * 180;
  var y = y / 20037508.34 * 180;
  y = 180 / Math.PI * (2 * Math.atan(Math.exp(y * Math.PI / 180)) - Math.PI / 2);
  return [x, y];
}

export const fromLonLat = (extent) => ol.proj.fromLonLat(extent);

export const toLonLat = (extent) => ol.proj.toLonLat(extent);

export const transPoints = (file) => {
  if (!file) return [];
  let txt, point;
  if (!file.includes(";")) {
    txt = file.replace(/\s/g, ';');
    point = txt.split(';');
    point.shift();
  } else {
    point = file.split(';');
  }
  point = point.map(item => item.split(','));
  return point.map(item => {
    item = [parseFloat(item[0]), parseFloat(item[1])];
    item = ol.proj.transform(item, EPSG4326, EPSG3857);
    return item;
  });
}

export const toPixel = (map, pixel) => map.getCoordinateFromPixel(pixel);

export const toCoordinate = (map, coordinate) => map.getPixelFromCoordinate(coordinate);

export function stringDivider(str, width, spaceReplacer) {
  if (!str) return '';
  if (str.length > width) {
      var p = width;
      while (p > 0 && (str[p] != ' ' && str[p] != '-')) {
          p--;
      }
      if (p > 0) {
          var left;
          if (str.substring(p, p + 1) == '-') {
              left = str.substring(0, p + 1);
          } else {
              left = str.substring(0, p);
          }
          var right = str.substring(p + 1);
          return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
      }
  }
  return str;
}