import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const { context, modules } = loadModules();
module.hot && module.hot.accept(context.id, update);

function update() {
  const { modules } = loadModules();
  store.hotUpdate({ modules });
}
function loadModules() {
  const context = require.context("@/store/modules", true, /\.js|.ts$/);
  const modulesFiles = context.keys();
  const config = modulesFiles.map((key) => ({ key, name: key.match(/^\.\/(.*)\.\w+$/i)[1] }));
  const modules = config.reduce( (cur, { key, name }) => ({ ...cur, [name]: context(key).default }), {});
  return { context, modules };
}
const store = new Vuex.Store({modules});
export default store;
